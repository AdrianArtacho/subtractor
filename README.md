This MaxForLive device was originally developed for the [Room107](https://bitbucket.org/AdrianArtacho/therocketthatbumpedofftheceiling/src/master/) interactive installation.

![Rocket_logo](https://bitbucket.org/AdrianArtacho/therocketthatbumpedofftheceiling/raw/HEAD/rocket_logo.jpeg)

# Subtractor

This device subtracts a mono signal (Inputs 3+4)
from the main Input 1+2 signal. Both signals need to be converted to mono 
if they are not mono already.

### Settings

![subtractor_neutral](https://docs.google.com/drawings/d/e/2PACX-1vQ3pcCwUVr04BtokfGssxT3aNl5-DWElpVARytPom2RBB4O1NGgNN3lt0T8A8_Y8Nxp-v0AVBISZTU6/pub?w=669&h=371)

These are the settings for the subtractor devices 
(if I remember correctly only the inputs 3+4 needed to be converted to mono, because the main track input is mono anyway)

### Broadcasting device

In order to save you the struggle of syncing all devices everytime,
choose one (for example _Mic 1_) and set it to braodacast its values to all other devices.

![Subtractor_broadcast](https://docs.google.com/drawings/d/e/2PACX-1vQnB3ks_HlOU5S2-qCTbt0LsDw9zwSB6vMs4JTDn9MXBBCgyskFFr2MbqmKncUnMoTygcZpMd2DpeXg/pub?w=666&h=376)

### Presets and dummies

The device should now save all values with the set. 
If it doesn't, at least the values are well labelled and 
you can insert dummies (C&P) where necessary. 
The value that I remember wasn't syncing fine was **DelayMS**, 
which should be set to zero, or near zero.

![subtractor_mappings](https://docs.google.com/drawings/d/e/2PACX-1vQ12cZbsavivU477UDYRnTtu9Ntav0hrhAfdkET3yA0eG0RU29Fkn0vLi0HNQnId75sA8THisgDYKn4/pub?w=323&h=503)
